from sklearn.ensemble import ExtraTreesClassifier
from sklearn.model_selection import GridSearchCV

def gridsearch(X, Y):
    model = GridSearchCV(estimator=ExtraTreesClassifier(), param_grid={})
    model.fit(X, Y)
    return model.best_estimator_.feature_importances_

print('GridSearchCV:')
token_importances = gridsearch(train_matrix, y_train)
print(token_importances.best_score_)